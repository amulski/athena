/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MdtDigitizationTool.h"
DECLARE_COMPONENT(MuonR4::MdtDigitizationTool)